﻿using System;
using System.Collections.Generic;
using System.Collections;
namespace jo
{
    class Program
    {
        static List<List<int>> graf = new List<List<int>>
        {
            new List<int>{1,3},
            new List<int>{0,2,3,4},
            new List<int>{1,3,4},
            new List<int>{0,1,2},
            new List<int>{1,2}
        };
        static List<List<int>> put = new List<List<int>>();
        static void Main(string[] args)
        {
            PoiskPuty(0, 4);
            foreach(List<int> a in put) 
            {
                foreach (int b in a)
                {
                    Console.Write(" {0}, ", b.ToString());
                }
                Console.WriteLine(" ");
            }
        }
        static void PoiskPuty(int nachalo, int konec)
        {
            put.Add(new List<int> { nachalo });
            Raschari();
        }
        static void Raschari()
        {
            List<List<int>> newput = new List<List<int>>();
            foreach(List<int>p in put)
            {
                int index = p[p.Count - 1];
                foreach(int s in graf[index])
                {
                    if (!p.Contains(s)) p.Add(s);
                }
            }
        }


    }
}
